package com.shinoby.ponggame;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager;

public class MainActivity extends Activity {

    private PongLogic mPongLogic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        mPongLogic= new PongLogic(this,size.x,size.y);
        setContentView(mPongLogic);
    }

    @Override
    protected void onResume(){
        super.onResume();
        mPongLogic.resume();
    }

    @Override
    protected void onPause(){
        super.onPause();
        mPongLogic.pause();
    }
}