package com.shinoby.ponggame;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class PongLogic extends SurfaceView implements Runnable{
    private final boolean DEBUGGING=true;

    private SurfaceHolder mySurfaceHolder;
    private Canvas myCanvas;
    private Paint myPaint;

    private long myFPS;

    private final int MILLIS_IN_SECOND = 1000;

    private int myScreenX;
    private int myScreenY;

    private int myFontSIze;
    private int myFontMargin;

    private Bat myBat;
    private Ball myBall;

    private int myScore;
    private int myLives;
    
    private Thread myGameThread = null; // le thread permet d'executer en parallele des actions
    private volatile boolean myPlaying;
    private boolean myPaused =true;

    public PongLogic(Context context, int x, int y){
        super(context);
        myScreenX=x;
        myScreenY=y;

        myFontSIze=myScreenX/20;
        myFontSIze=myScreenY/75;

        mySurfaceHolder=getHolder();
        myPaint=new Paint();

        myBall.getMyRect();
        startNewGame();
    }

    private void startNewGame(){
        myBall.reset(myScreenX,myScreenY);

        myScore=0;
        myLives=3;
    }

    private void draw(){
        if (mySurfaceHolder.getSurface().isValid()){
            myCanvas=mySurfaceHolder.lockCanvas();//initialize le canvas
            myCanvas.drawColor(Color.argb(255,26,128,182));
            myPaint.setColor(Color.argb(255,255,255,255));

            myCanvas.drawRect(myBall.getMyRect(),myPaint);
            
            myPaint.setTextSize(myFontSIze);
            myCanvas.drawText("Score"+myScore+" Vies "+myLives,myFontMargin,myFontSIze,myPaint);
            if (DEBUGGING){
                printDebuggingText();
            }
            mySurfaceHolder.unlockCanvasAndPost(myCanvas);
        }
    }

    private void printDebuggingText() {
        int debugSize = myFontSIze/2;
        int debugStart = 150;
        myPaint.setTextSize(debugSize);
        myCanvas.drawText("FPS: "+myFPS,10,debugStart+debugSize,myPaint);
    }

    @Override
    public void run() {
        while(myPlaying){
            long frameStartTime = System.currentTimeMillis();
            if(!myPaused){
                update();
                detectCollisions();
            }
            draw();
            long timeThisFrame = System.currentTimeMillis()-frameStartTime;  //calculer le temps de la boucle
            if(timeThisFrame>0){
                myFPS= MILLIS_IN_SECOND/timeThisFrame; //calculer le FPS
            }
        }
    }

    private void detectCollisions() {
    }

    private void update() {
        myBall.update(myFPS);
    }

    public void pause(){
        myPlaying = false;
        try {
            myGameThread.join();
        } catch (InterruptedException e){
            Log.e("Erreur","thread injoignable");
        }
    }
    
    public void resume(){
        myPlaying = true;
        myGameThread=new Thread(this);
        myGameThread.start();
    }
}
