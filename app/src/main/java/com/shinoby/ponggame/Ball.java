package com.shinoby.ponggame;

import android.graphics.RectF;

public class Ball {

    public RectF myRect;
    private float myXVelocity;
    private float myYVelocity;
    private float myBallWidth;
    private float myBallHeight;

    public Ball(int ScreenX){
        myBallWidth=ScreenX/100;
        myBallHeight=ScreenX/100;

        myRect = new RectF();
    }
    public RectF getMyRect() {
        return myRect;
    }

    public void update(long fps) {
        myRect.left=myRect.left+(myXVelocity/fps);
        myRect.top=myRect.top+(myYVelocity/fps);

        myRect.right=myRect.left+myBallWidth;
        myRect.bottom=myRect.top+myBallHeight;
    }
    void reverseYVelocity(){
        myYVelocity=-myYVelocity;
    }
    void reverseXVelocity(){
        myXVelocity=-myXVelocity;
    }
    void reset(int x, int y){
        myRect.left=x/2;
        myRect.top=0;
        myRect.right=x/2+myBallWidth;
        myRect.bottom=myBallHeight;

        myYVelocity=-(y/3);
        myXVelocity=(x/2);
    }

    void increaseVelocity(){
        myXVelocity=myXVelocity*1.1f;
        myYVelocity=myYVelocity*1.1f;
    }

    void batBounce(RectF batPosition){
        float batCenter = batPosition.left+(batPosition.width()/2);
        float ballCenter = myRect.left+(myBallWidth/2);
        float relativeInersect = (batCenter-ballCenter);

        if (relativeInersect<0){
            myXVelocity=Math.abs(myXVelocity);
        }else {
            myXVelocity=-Math.abs(myXVelocity);
        }
        reverseYVelocity();
    }
}
